package com.Inyeccion.demo.Models.domain;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
public class Factura {

	@Autowired
	private Cliente cliente;
	@Autowired
	@Qualifier("itemsFactura")
	private List<Itemfactura> item;
	@Value("${descripcion.factura}")
	private String Descripcion;

	@PostConstruct
	public void Inicializar () {
		cliente.setNombre(cliente.getNombre().concat("").concat("Jose"));
		Descripcion = Descripcion.concat(" del cliente :").concat(cliente.getNombre());
	}
	
	public void destruir () {
		
		System.out.println("Factura destruida ".concat(Descripcion));
		
	}
	
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Itemfactura> getItem() {
		return item;
	}

	public void setItem(List<Itemfactura> item) {
		this.item = item;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

}
