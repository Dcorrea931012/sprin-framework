package com.Inyeccion.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringInyeccionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringInyeccionApplication.class, args);
	}

}
