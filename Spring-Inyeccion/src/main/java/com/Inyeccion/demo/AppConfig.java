package com.Inyeccion.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.Inyeccion.demo.Models.domain.Itemfactura;
import com.Inyeccion.demo.Models.domain.Producto;

@Configuration
public class AppConfig {

	
	@Bean("itemsFactura")
	public List<Itemfactura> registrarItems(){
		Producto producto1 = new Producto("camara sony", 25000);
		Producto producto2 = new Producto("Bicicleta TKM", 60000);
		Itemfactura item1 = new Itemfactura(producto1, 5);
		Itemfactura item2= new Itemfactura(producto2, 10);
		return Arrays.asList(item1,item2);
		
		
	}
	
	
	

	@Bean("itemsFacturaOficina")
	public List<Itemfactura> registrarItemsOficina(){
		Producto producto1 = new Producto("monitor lCD desarrollo", 65000);
		Producto producto2 = new Producto("netbook asus Vivo", 12000);
		Producto producto3 = new Producto("impresora HP funcional", 8000);
		Producto producto4 = new Producto("Escritorio de oficina", 6000);
		
		Itemfactura item1 = new Itemfactura(producto1, 5);
		Itemfactura item2= new Itemfactura(producto2, 10);
		Itemfactura item3 = new Itemfactura(producto3, 5);
		Itemfactura item4= new Itemfactura(producto4, 10);
		return Arrays.asList(item1,item2,item3,item4);
		
		
	}
	
}
